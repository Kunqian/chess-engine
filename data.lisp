(load "~/chess/data2.lisp")

(defun reduce1 (f lst)
  (if (null lst) lst
    (funcall f (car lst) (reduce f (cdr lst)))))

(defvar h '())

(defmacro add-to (m v h) `(setf ,h (cons (cons ,m ,v) ,h)))


(loop for cord below 8 do
      (loop for amap from 1 to 255 do
	    (if (not (= 0 (logand amap (nth cord cmap))))
		(let ((cord1 cord)
		      (cord2 cord))
		  (loop
		   (if (<= cord1 0) (return))
		   (setf cord1 (- cord1 1))
		   (if (not (= 0 (logand (nth cord1 cmap) amap))) (return)))
		  (loop
		   (if (>= cord2 7) (return))
		   (setf cord2 (+ cord2 1))
		   (if (not (= 0 (logand (nth cord2 cmap) amap))) (return)))
		  (let ((map00 (ash amap 56))
			(val (logior (aref fromToRay cord cord1)
				     (aref fromToRay cord cord2))))
		    (setAttack attack00 cord map00 val)

		    (let ((map90 (reduce 'logior 
					 (do-bits (c map00)
						  (collect (ash 1 (- 63 
								     (aref rot1 c)))))))
			  (val (logior (aref fromToRay 
					     (aref rot1 cord) 
					     (aref rot1 cord1))
				       (aref fromToRay
					     (aref rot1 cord) 
					     (aref rot1 cord2)))))
		      (setAttack attack90 (aref rot1 cord) map90 val))		
		    (let ((map45 (reduce 'logior 
					 (do-bits (c map00)
						  (collect (ash 1 (- 63 
								   (aref rot2 c)))))))
			  (val (logior (aref fromToRay 
					     (aref rot2 cord) 
					     (aref rot2 cord1))
				       (aref fromToRay
					     (aref rot2 cord) 
					     (aref rot2 cord2)))))
;;		      (print map45)
		      (setAttack attack45 (aref rot2 cord) map45 val)
		      (if (and  
			       (= 512 map45)
			       (= 9241421688590303233 val))
			  (print (list map45 val (aref rot2 cord))))
		      (if (= (aref rot2 cord) 63)
			  (add-to map45 val h))
;;		      (print val)
		      )
		    (let ((map135 (reduce 'logior 
					  (do-bits (c map00)
						   (collect (ash 1 (- 63 
								      (aref rot3 c)))))))
			  (val (logior (aref fromToRay 
					     (aref rot3 cord) 
					     (aref rot3 cord1))
				       (aref fromToRay
					     (aref rot3 cord) 
					     (aref rot3 cord2)))))
		      (setAttack attack135 (aref rot3 cord) map135 val)))))))

(defun print-hash-entry (key value)
    (format t "The value associated with the key ~S is ~S~%" key value))

(defvar MAXBITBOARD (- (ash 1 64) 1))

(do ((r A2 (+ 8 r)))
    ((>= r A8))
  (do-bits (cord (aref ray00 r))
	   (maphash #'(lambda (k v) (setAttack attack00 
					       cord 
					       (ash k -8) 
					       (ash v -8))) 
		    (aref attack00 (- cord 8)))))

(do ((r B1 (+ 1 r)))
    ((> r H1))
  (do-bits (cord (aref ray90 r))
	   (maphash #'(lambda (k v) (setAttack attack90 
					       cord 
					       (ash k -1) 
					       (ash v -1))) 
		    (aref attack90 (mod (- cord 1) 64)))))

(do ((r B1 (+ 1 r)))
    ((> r H1))
  (do-bits (cord (aref ray45 r))
	   (maphash #'(lambda (k v) (setAttack attack45
					       cord 
					       (logand (ash k 8) MAXBITBOARD) 
					       (logand (ash v 8) MAXBITBOARD))) 
		    (aref attack45 (mod (+ cord 8) 64)))))



(do ((r (- H8 1) (- r 1)))
    ((< r A8))
  (do-bits (cord (aref ray45 r))
	   (maphash #'(lambda (k v) (setAttack attack45
					       cord 
					       (ash k -8) 
					       (ash v -8))) 
		    (aref attack45 (- cord 8)))))

(do ((r B8 (+ 1 r)))
    ((> r H8))
  (do-bits (cord (aref ray135 r))
	   (maphash #'(lambda (k v) (setAttack attack135
					       cord  
					       (ash k -8) 
					       (ash v -8))) 
		    (aref attack135 (mod (- cord 8) 64)))))

(do ((r (- H1 1) (- r 1)))
    ((< r A1))
  (do-bits (cord (aref ray135 r))
	   (maphash #'(lambda (k v) (setAttack attack135 
					       cord 
					       (logand (ash k 8) MAXBITBOARD) 
					       (logand (ash v 8) MAXBITBOARD))) 
		    (aref attack135 (+ cord 8)))))
