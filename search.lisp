(load "~/chess/movegen.lisp")

(defun simple-eval (b)
  (let* ((color (color b))
	 (copy b)
	 (boards (boards b))
	 (wki (bitLength (aref boards white king)))
	 (bki (bitLength (aref boards black king)))
	 (wq (bitLength (aref boards white queen)))
	 (bq (bitLength (aref boards black queen)))
	 (wr (bitLength (aref boards white rook)))
	 (br (bitLength (aref boards black rook)))
	 (wb (bitLength (aref boards white bishop)))
	 (bb (bitLength (aref boards black bishop)))
	 (wk (bitLength (aref boards white knight)))
	 (bk (bitLength (aref boards black knight)))
	 (wp (bitLength (aref boards white pawn)))
	 (bp (bitLength (aref boards black pawn)))
         (wm (progn  
	       (setColor copy white) 
	       (length (generate-all-moves copy))))
	 (bm (progn  
	       (setColor copy black) 
	       (length (generate-all-moves copy))))
	 (val (+ (* 200 (- wki bki))
		 (* 9 (- wq bq))
		 (* 5 (- wr br))
		 (* 3 (+ (- wb bb) (- wk bk)))
		 (* 1 (- wp bp))
		 (* 0.1 (- wm bm)))))
    (setColor copy color)
    val
   ))

(defvar fen "6k1/r7/4n1Q1/8/6K1/2bb4/8/8 b - - 0 0")

(defvar fen2 "4k3/8/8/8/8/8/8/4K2R w - - 0 1")

(defvar fen3 "r3k3/8/8/8/3p4/8/4P3/4K2R w Kq - 0 1")

(defvar fen4 "r3k3/8/8/8/3R4/8/4P3/4K3 w - - 0 1")

(defvar fen5 "4k3/8/8/8/8/8/4P3/4K3 w - - 0 1")

(defun alphaBeta (b alpha beta ply)
  (if (= ply 0)
    (simple-eval b)
    (let* ((isCheck (isChecked b))
	   (moves (if isCheck
		      (genCheckEvasions b)
		    (generate-all-moves b)))
	   (best-move (first moves)))
           
      (loop for move in moves do
	    (print move)
	    (print (print-board (blocker b)))
	    (print (arBoard b))
	    (applyMove b move)
	    (if (not isCheck)
		(if (opIsChecked b)
		    (popMove b)       
		  (let ((val (- (alphaBeta b (- beta) (- alpha) (- ply 1)))))
;;		    (print (print-board (blocker b)))
		    (popMove b)
		    (when (> val alpha)
		      (setf alpha val)
		      (setf best-move move))))
	      (let ((val (- (alphaBeta b (- beta) (- alpha) (- ply 1)))))
;;		(print (print-board (blocker b)))
;;		(print move)
		(popMove b)
		(when (> val alpha)
		  (setf alpha val)
		  (setf best-move move))))
	    until (>= alpha beta))
      (values alpha best-move))))


(defun getfrom (move) (logand (ash move -6) 63))
(defun getto (move) (logand move 63))
(defun getflag (move) (ash move -12))